## Overview
* Cancer is known to result from a combination of a small number of genetic defects. 
* The specific combinations of mutations responsible for the vast majority of cancers have not been identified. 
* We present a fundamentally different approach for identifying the cause of individual instances of cancer: we search for combinations of carcinogenic mutations (multi-hit combinations) instead of driver mutations. 
* We developed an algorithm that identified a set of multi-hit combinations that differentiate between tumor and normal tissue samples with $91\%$ sensitivity (95% Confidence Interval (CI)=89-92%) and 93%  specificity (95% CI=91-94%) on average for seventeen cancer types. 

## Identify Combinations for One Cancer Type

> cd src 
>
> ./compute-multihit-combinations.sh BRCA


---

## Output
The output combinations are in the folder result/BRCA

---

